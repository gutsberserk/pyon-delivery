from datetime import date


class UserData:
    common_user = {
        'username': 'user',
        'first_name': 'test',
        'last_name': 'example',
        'email': 'example@mail.com',
        'phone': '5511933334444',
        'cpf': '44455566611',
        'password': '1234',
        'image_url': 'static/user.png',
        'birthday_date': '2000-12-01',
        'is_admin': False,
        'is_manager': False
    }

    common_user_2 = {
        'username': 'anotheruser',
        'first_name': 'test',
        'last_name': 'example',
        'email': 'another@mail.com',
        'phone': '5511933335555',
        'cpf': '44455566622',
        'password': '1234',
        'image_url': 'static/user.png',
        'birthday_date': '2000-12-01',
        'is_admin': False,
        'is_manager': False
    }

    admin_user = {
        'username': 'admin',
        'first_name': 'test',
        'last_name': 'example',
        'email': 'admin@mail.com',
        'phone': '5511955554444',
        'cpf': '44455566622',
        'password': '1234',
        'image_url': 'static/user.png',
        'birthday_date': '2000-12-01',
        'is_admin': True,
        'is_manager': False
    }

    manager_user = {
        'username': 'manager',
        'first_name': 'test',
        'last_name': 'example',
        'email': 'manager@mail.com',
        'phone': '5511966667777',
        'cpf': '44455566633',
        'password': '1234',
        'image_url': 'static/user.png',
        'birthday_date': '2000-12-01',
        'is_admin': False,
        'is_manager': True
    }
