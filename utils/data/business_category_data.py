class BusinessCategoryData:
    restaurant_category_data = {
        'name': 'restaurant',
        'description': 'A small italian restaurant',
    }

    market_category_data = {
        'name': 'hypermarket',
        'description': 'A big market',
    }
    