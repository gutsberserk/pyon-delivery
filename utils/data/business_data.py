class BusinessData:
    restaurant_data = {
        'name': 'example',
        'type': 'restaurant',
        'cnpj': '12345678000122',
        'corporate_email': 'example@mail.com',
        'website': 'example.com',
        'phone': '5511988880000',
        'active_employees': 12
    }
    