from django.db import models
import uuid

class Product(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    price = models.FloatField()
    unit_type = models.CharField(max_length=255)
    unit_value = models.IntegerField()
    description = models.TextField()
    ingredients = models.CharField(max_length=255,null=True)
    production_time = models.TimeField(null=True)
    business = models.ForeignKey("business.Business", on_delete=models.CASCADE)
