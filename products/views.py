from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from carts.serializers import CartProductSerializer, CartSerializer
from orders.models import Order

from users.serializers import UserSerializer
from users.models import User
from .serializers import ProductSerializer
from .models import Product
from business.models import Business
from carts.models import CartProduct, Cart

class CreateListProductView(APIView):
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]

    def post(self, request):
        
        business_id = request.data["business_id"]

        business = Business.objects.get(id=business_id)
        
        if request.user.id != business.user_id:
            return Response({"message":"Não é possível realizar operação sobre esse business porque ele não pertence a voce"})

        product_serializer = ProductSerializer(data=request.data)
        print(product_serializer.is_valid())
        if product_serializer.is_valid():
            product_serializer.save()
            return Response(product_serializer.data, status=201)
        
        return Response("Erro")

    def get(self, request):

        product = Product.objects.all()

        product_serializer = ProductSerializer(product, many=True)

        return Response(product_serializer.data)
    
class ListProductBelongUser(APIView):

    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]

    def get(self, request, business_id=""):

        try:
            business = Business.objects.filter(id=business_id).first()
            if not business:
                raise Http404
        except:
            raise Http404

        products = Product.objects.filter(business_id=business_id)

        products_serializer = ProductSerializer(products, many=True)

        return Response(products_serializer.data)

class UpdateDeleteProductAdmin(APIView):

    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]

    def patch(self, request, product_id=''):

        if request.user.is_admin == False:
            return Response({"message": "adm permissions"})

        Product.objects.filter(id=product_id).update(**request.data)

        product = Product.objects.get(id=product_id)
        product_serializer = ProductSerializer(product)

        return Response(product_serializer.data)

    def delete(self, request, product_id=''):
        if request.user.is_admin == False:
            return Response({"message": "adm permissions"})
        
        return Response(status=204)

class UpdateDeleteProductFromUser(APIView):

    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]

    def patch(self, request, product_id=''):
        product = Product.objects.get(id=product_id)
        
        business = Business.objects.get(id=product.business_id)

        if request.user.id != business.user_id:
            return Response({"message":"Não é possível realizar operação sobre esse business porque ele não pertence a voce"})

        Product.objects.filter(id=product_id).update(**request.data)

        product = Product.objects.get(id=product_id)
        product_serializer = ProductSerializer(product)

        return Response(product_serializer.data)

    def delete(self, request, product_id=''):
        product = Product.objects.get(id=product_id)
        
        business = Business.objects.get(id=product.business_id)

        if request.user.id != business.user_id:
            return Response({"message":"Não é possível realizar operação sobre esse business porque ele não pertence a voce"})

        product.delete()

        return Response(status=204)

class AddedProductInCart(APIView):

    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]  

    def post(self, request, product_id = ""):

        user_id = request.user.id
    
        if not request.user.cart_id:
            cart = Cart.objects.create()        
            user = User.objects.get(id=user_id)
            user.cart = cart    
            user.save()

        user = User.objects.get(id=user_id)
        user_serializer = UserSerializer(user)

        try:
            product = Product.objects.get(id=product_id)
        except:
            return Response("ERRO")
        cart = Cart.objects.get(id=user.cart_id)
        
        cart.products.add(product)

        return Response({"message": "product added to cart" })

class ListProductsInCart(APIView):
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]

    def get(self, request):
        
        try:
            cart = Cart.objects.get(id=request.user.cart_id)
        except:
            raise Http404

        carts_serializer = CartSerializer(cart)
        
        return Response(carts_serializer.data["products"])
        
class CreateOrderByCartProducts(APIView):

    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]

    def post(self, request):

        try:
            cart = Cart.objects.filter(id=request.user.cart_id).first()
            if not cart:
                raise Http404
        except:
            raise Http404

        cart_serialized = CartSerializer(cart).data

        if cart_serialized['products'] == []:
            return Response({'message': 'empty cart'}, status=404)

        unique_business = []

        for product in cart_serialized['products']:
            product_data = ProductSerializer(product).data
            unique_business.append(product_data['business_id'])

        for business in set(unique_business):
            order = Order.objects.create(user=request.user, business_id=business)
            for product in cart_serialized['products']:
                product_data = ProductSerializer(product).data
                if product_data['business_id'] == business:
                    product_to_add = Product.objects.get(id=product_data['id'])
                    order.products.add(product_to_add)
            
        cart.products.clear()

        return Response({'message': 'Orders created successfully'})