from django import urls
from django.urls import path
from .views import CreateOrderByCartProducts, ListProductsInCart ,AddedProductInCart, CreateListProductView, UpdateDeleteProductAdmin,ListProductBelongUser, UpdateDeleteProductFromUser

urlpatterns = [
    path("products/", CreateListProductView.as_view()),
    path("products/business/<str:business_id>/", ListProductBelongUser.as_view()),
    path("products/user/<str:product_id>/", UpdateDeleteProductFromUser.as_view()),
    path("products/admin/<str:product_id>/", UpdateDeleteProductAdmin.as_view()),
    path("products/cart/", ListProductsInCart.as_view()),
    path("products/cart/<str:product_id>/", AddedProductInCart.as_view()),
    path("products/buy/", CreateOrderByCartProducts.as_view()),
]