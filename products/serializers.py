from dataclasses import fields
from pyexpat import model
from rest_framework import serializers
from .models import Product

class ProductSerializer(serializers.Serializer):
    id = serializers.UUIDField(read_only=True)
    name = serializers.CharField()
    price = serializers.FloatField()
    unit_type = serializers.CharField()
    unit_value = serializers.IntegerField()
    description = serializers.CharField()
    ingredients = serializers.CharField(required=False)
    production_time = serializers.TimeField(required=False)
    business_id = serializers.UUIDField()

    def create(self, validated_data):
        return Product.objects.create(**validated_data)
