from django.db import models
import uuid


class Review(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    body = models.TextField()
    rating = models.FloatField(default=1.0)
    created_at = models.DateTimeField(auto_now_add=True)
    business_response = models.TextField(null=True)
    user = models.ForeignKey("users.User", on_delete=models.CASCADE)
    business = models.ForeignKey("business.Business", on_delete=models.CASCADE)
