from django.test import TestCase
from reviews.models import Review

from users.models import User
from business.models import Business
from utils.data.user_data import UserData
from utils.data.business_data import BusinessData


class ReviewsModelTest(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.body = 'A simple review'
        cls.rating = 5.0

        cls.user = User.objects.create_user(**UserData.common_user)
        cls.business = Business.objects.create(**BusinessData.restaurant_data, user=cls.user)

        cls.review = Review.objects.create(
            body = cls.body,
            rating = cls.rating,
            user = cls.user,
            business = cls.business
        )

    def test_reviews_fields(self):
        self.assertIsInstance(self.review.body, str)
        self.assertEqual(self.review.body, self.body)
        self.assertIsInstance(self.review.rating, float)
        self.assertEqual(self.review.rating, self.rating)
        self.assertIsInstance(self.review.user, User)
        self.assertEqual(self.review.user.username, self.user.username)
        self.assertIsInstance(self.review.business, Business)
        self.assertEqual(self.review.business.name, self.business.name)
        