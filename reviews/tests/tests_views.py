
from uuid import uuid4
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from reviews.models import Review
from users.models import User
from business.models import Business
from utils.data.review_data import ReviewData

from utils.data.user_data import UserData
from utils.data.business_data import BusinessData


class TestReviewsRoutes(APITestCase):

    def test_common_user_review_creation_returns_201(self):
        common_user = User.objects.create_user(**UserData.common_user)
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        review_data = ReviewData.good_review_data

        response = self.client.post(f'/api/reviews/business/{business.id}/', review_data)
        output = response.json()

        self.assertEqual(response.status_code, 201)
        self.assertEqual(output['body'], review_data['body'])
        self.assertIn('id', output)

    def test_special_user_try_to_review_creation_returns_403(self):
        admin_user = User.objects.create_user(**UserData.admin_user)
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        review_data = ReviewData.good_review_data

        response = self.client.post(f'/api/reviews/business/{business.id}/', review_data)
        
        self.assertEqual(response.status_code, 403)

    def test_common_user_try_to_review_creation_with_invalid_rating_returns_400(self):
        common_user = User.objects.create_user(**UserData.common_user)
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        review_data = ReviewData.invalid_rating_review_data

        response = self.client.post(f'/api/reviews/business/{business.id}/', review_data)

        self.assertEqual(response.status_code, 400)

    def test_common_user_try_to_review_creation_with_nonexistent_business_returns_404(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_id = uuid4()

        review_data = ReviewData.good_review_data

        response = self.client.post(f'/api/reviews/business/{business_id}/', review_data)

        self.assertEqual(response.status_code, 404)

    def test_any_user_list_all_reviews_of_any_business_returns_200(self):
        common_user = User.objects.create_user(**UserData.common_user)
        manager_user = User.objects.create_user(**UserData.manager_user)
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        review_data = ReviewData.good_review_data
        review = Review.objects.create(**review_data, user=common_user, business=business)

        response = self.client.get(f'/api/reviews/business/{business.id}/')
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output[0]['body'], review.body)
        self.assertEqual(output[0]['user']['id'], str(common_user.id))
        self.assertEqual(output[0]['business']['id'], str(business.id))

    def test_any_user_try_to_list_all_reviews_of_nonexistent_business_returns_404(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_id = uuid4()

        response = self.client.get(f'/api/reviews/business/{business_id}/')

        self.assertEqual(response.status_code, 200)

    def test_common_user_retrieve_own_review_returns_200(self):
        common_user = User.objects.create_user(**UserData.common_user)
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        review_data = ReviewData.good_review_data
        review = Review.objects.create(**review_data, user=common_user, business=business)

        response = self.client.get(f'/api/reviews/user/{review.id}/')
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output['body'], review.body)

    def test_common_user_try_to_retrieve_other_user_review_returns_403(self):
        common_user = User.objects.create_user(**UserData.common_user)
        another_user = User.objects.create_user(**UserData.common_user_2)
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        review_data = ReviewData.good_review_data
        review = Review.objects.create(**review_data, user=another_user, business=business)

        response = self.client.get(f'/api/reviews/user/{review.id}/')

        self.assertEqual(response.status_code, 403)

    def test_common_user_try_to_retrieve_nonexistent_review_returns_404(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        review_id = uuid4()

        response = self.client.get(f'/api/reviews/user/{review_id}/')

        self.assertEqual(response.status_code, 404)

    def test_common_user_update_own_review_returns_200(self):
        common_user = User.objects.create_user(**UserData.common_user)
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        review_data = ReviewData.good_review_data
        review = Review.objects.create(**review_data, user=common_user, business=business)

        update_data = {
            'body': 'Updated review'
        }

        response = self.client.patch(f'/api/reviews/user/{review.id}/', update_data)
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output['body'], update_data['body'])

    def test_manager_reply_own_business_review_returns_200(self):
        common_user = User.objects.create_user(**UserData.common_user)
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        review_data = ReviewData.good_review_data
        review = Review.objects.create(**review_data, user=common_user, business=business)

        business_response = {
            'business_response': 'Thanks for your support'
        }

        response = self.client.put(f'/api/reviews/manager/{review.id}/', business_response, format='json')
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output['business_response'], business_response['business_response'])

    def test_manager_try_to_reply_other_user_business_review_returns_403(self):
        common_user = User.objects.create_user(**UserData.common_user)
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=common_user)

        review_data = ReviewData.good_review_data
        review = Review.objects.create(**review_data, user=common_user, business=business)

        business_response = {
            'business_response': 'Thanks for your support'
        }

        response = self.client.put(f'/api/reviews/manager/{review.id}/', business_response, format='json')

        self.assertEqual(response.status_code, 403)

    def test_manager_try_to_reply_nonexistent_business_review_returns_404(self):
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_response = {
            'business_response': 'Thanks for your support'
        }

        review_id = uuid4()

        response = self.client.put(f'/api/reviews/manager/{review_id}/', business_response, format='json')

        self.assertEqual(response.status_code, 404)
