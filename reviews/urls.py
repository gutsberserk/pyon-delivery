from django.urls import path

from reviews.views import ManagerReviewReplyView, ReviewCreateAndListView, ReviewEditView

urlpatterns = [
    path('reviews/business/<business_id>/', ReviewCreateAndListView.as_view()),
    path(
        'reviews/user/<review_id>/',
        ReviewEditView.as_view(),
    ),
    path('reviews/manager/<review_id>/', ManagerReviewReplyView.as_view()),
]
