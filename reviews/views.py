from django.http import Http404
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from pyonDelivery.permissions import OnlyCommonUserCreateEditReview, OnlyReviewBusinessOwner, OnlyReviewOwner
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from rest_framework import status

from reviews.models import Review
from reviews.serializers import ReviewBusinessResponsePutSerializer, ReviewEditSerializer, ReviewSerializer


class ReviewCreateAndListView(ListCreateAPIView):

    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    lookup_url_kwarg = 'business_id'

    permission_classes = [OnlyCommonUserCreateEditReview]
    authentication_classes = [TokenAuthentication]


class ReviewEditView(RetrieveUpdateDestroyAPIView):

    queryset = Review.objects.all()
    serializer_class = ReviewEditSerializer
    lookup_url_kwarg = 'review_id'

    permission_classes = [OnlyReviewOwner]
    authentication_classes = [TokenAuthentication]

class ManagerReviewReplyView(APIView):

    permission_classes = [OnlyReviewBusinessOwner]
    authentication_classes = [TokenAuthentication]   

    def put(self, request, review_id=''):
        
        try:
            review = Review.objects.filter(id=review_id).first()
            if not review:
                raise Http404 
        except:
            raise Http404 

        try:
            business_response = request.data['business_response']
            if not isinstance(business_response, str):
                return Response({"business_response": "Invalid type. The input needs to be a string."}, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({"business_response": "Missing parameter."}, status=status.HTTP_400_BAD_REQUEST)

        Review.objects.filter(id=review_id).update(**{
            'business_response': business_response
        })

        review = Review.objects.filter(id=review_id).first()

        serialized = ReviewBusinessResponsePutSerializer(review)

        return Response(serialized.data, status=status.HTTP_200_OK)
