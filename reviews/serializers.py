from django.http import Http404
from rest_framework.serializers import ModelSerializer
from business.models import Business
from business.serializers import BusinessSerializer
from pyonDelivery.exceptions import InvalidRatingRangeException
from rest_framework import serializers

from reviews.models import Review
from users.serializers import UserSerializer


class ReviewSerializer(ModelSerializer):

    user = UserSerializer(read_only=True)
    business = BusinessSerializer(read_only=True)

    class Meta:
        model = Review
        fields = '__all__'

    def validate(self, attrs):

        rating = attrs['rating']

        if rating < 1 or rating > 5:
            raise InvalidRatingRangeException()

        attrs['user'] = self.context['request'].user

        kwargs = self.context['view'].kwargs

        business_id = kwargs['business_id']

        try:
            business = Business.objects.filter(id=business_id).first()
            if not business:
                raise Http404
        except:
            raise Http404

        attrs['business'] = business

        return super().validate(attrs)

    def create(self, validated_data):
        return Review.objects.create(**validated_data)
        

class ReviewEditSerializer(ModelSerializer):

    business = BusinessSerializer(read_only=True)

    class Meta:
        model = Review
        fields = ['body', 'rating', 'business', 'business_response']

        extra_kwargs = {
            'business_response': {
                'read_only': True
            }
        }

    def validate(self, attrs):

        if 'rating' in attrs:
            rating = attrs['rating']

            if rating < 1 or rating > 5:
                raise InvalidRatingRangeException()

        return super().validate(attrs)


class ReviewBusinessResponsePutSerializer(serializers.Serializer):
    id = serializers.UUIDField(read_only=True)
    body = serializers.CharField()
    rating = serializers.FloatField()
    business_response = serializers.CharField()
    user = UserSerializer()
    business = BusinessSerializer()
