#!/usr/bin/env bash
# start-server.sh
python manage.py migrate
chown www-data:www-data /opt/app/db.sqlite3
if [ -n "$DJANGO_SUPERUSER_USERNAME" ] && [ -n "$DJANGO_SUPERUSER_PASSWORD" ] ; then
    (python manage.py createsuperuser --no-input)
fi
(gunicorn pyonDelivery.wsgi --user www-data --bind 0.0.0.0:8010 --workers 3) &
nginx -g "daemon off;"
