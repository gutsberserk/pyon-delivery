from django import urls
from django.urls import path
from .views import SendEmailView

urlpatterns = [
    path("reports/", SendEmailView.as_view()),
]
