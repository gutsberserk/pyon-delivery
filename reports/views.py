from rest_framework.views import APIView
from rest_framework.response import Response
import smtplib
from dotenv import dotenv_values

config = dotenv_values(".env")

class SendEmailView(APIView):

    def post(self, request):
        try:
            body = request.data["body"]

            if type(body) != str:
                return Response({'message': "campo body deve ser uma string."},status = 400)

        except KeyError:
            return Response({'message': "campo body obrigatorio."},status = 400)
            
        host_username = config['host_username']
        host_password = config['host_password']

        server = smtplib.SMTP('smtp.mailtrap.io', 2525)

        server.starttls()

        server.login(host_username, host_password)
        
        message = f"""From: PyOn Delivery <pyondelivery@pyon.com>
            To: manager <manager@mail.com>
            Subject: Relatório de gerenciamento

            {body}
            """

        usern = 'yourusername@inbox.mailtrap.io'

        server.sendmail(usern, usern, message)

        return Response({}, status=201)