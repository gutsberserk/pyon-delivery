from django.http import Http404
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from business_categories.models import BusinessCategory

from pyonDelivery.permissions import OnlyAdmin, OnlyManagerCreate, OnlyOwner, OnlyManagerOwnerAddCategory
from .models import Business

from business.serializers import BusinessSerializer, CategoriesOfBusinessSerializer

class CreateListBusinessView(ListCreateAPIView):

    queryset = Business.objects.all()
    serializer_class = BusinessSerializer

    permission_classes = [OnlyManagerCreate]
    authentication_classes = [TokenAuthentication]

    
class ListBusinessBelongUser(APIView):

    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]

    def get(self, request):
        
        id = request.user.id

        business = Business.objects.filter(user_id=id)
        
        serializer_business = BusinessSerializer(business, many=True)
        
        return Response(serializer_business.data)

class UpdateDeleteBusinessAdmin(RetrieveUpdateDestroyAPIView):

    queryset = Business.objects.all()
    serializer_class = BusinessSerializer
    lookup_url_kwarg = 'business_id'

    permission_classes = [OnlyAdmin]
    authentication_classes = [TokenAuthentication]
    

class UpdateDeleteBusinessFromUser(RetrieveUpdateDestroyAPIView):

    queryset = Business.objects.all()
    serializer_class = BusinessSerializer
    lookup_url_kwarg = 'business_id'

    permission_classes = [OnlyOwner]
    authentication_classes = [TokenAuthentication]


class ListAddCategoriesView(APIView):

    permission_classes = [OnlyManagerOwnerAddCategory]
    authentication_classes = [TokenAuthentication]

    def get(self, request, business_id=''):

        try:
            business = Business.objects.filter(id=business_id).first()
            if not business:
                raise Http404 
        except:
            raise Http404 

        serialized = CategoriesOfBusinessSerializer(business)

        return Response(serialized.data)

    def put(self, request, business_id=''):

        try:
            business = Business.objects.filter(id=business_id).first()
            if not business:
                raise Http404 
        except:
            raise Http404 

        try:
            categories_name = request.data['categories']
            if not isinstance(categories_name, list):
                return Response({"categories": "Invalid type. The input needs to be a list."}, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({"categories": "Missing parameter."}, status=status.HTTP_400_BAD_REQUEST)
         
        categories = []
        for category_name in request.data['categories']:
            try:
                category = BusinessCategory.objects.filter(name=category_name).first()
                if not category:
                    raise Http404
                categories.append(category)
            except:
                raise Http404

        business.categories.clear()

        for category in categories:
            business.categories.add(category)

        serialized = CategoriesOfBusinessSerializer(business)

        return Response(serialized.data, status=status.HTTP_200_OK)


class AddAddressBusiness(APIView):
    def put(self, request, business_id=''):
        return Response({"message": "address added"})


