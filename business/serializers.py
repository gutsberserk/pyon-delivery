from rest_framework import serializers

from rest_framework.serializers import ModelSerializer

from business_categories.serializers import BusinessCategorySerializer

from .models import Business


class BusinessSerializer(ModelSerializer):

    class Meta:
        model = Business
        fields = ['id', 'name', 'type', 'cnpj', 'corporate_email', 'website', 'phone', 'active_employees']

    def validate(self, attrs):

        attrs['user'] = self.context['request'].user

        return super().validate(attrs)


class CategoriesOfBusinessSerializer(serializers.Serializer):
    id = serializers.UUIDField(read_only=True)
    categories = BusinessCategorySerializer(many=True)
    