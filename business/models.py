from django.db import models
import uuid
from users.models import User

class Business(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    cnpj = models.CharField(max_length=255, unique=True)
    corporate_email = models.EmailField(max_length=255, null=True)
    website = models.CharField(max_length=255, null=True)
    phone = models.CharField(max_length=255, null=True)
    active_employees = models.IntegerField(null=True)

    categories = models.ManyToManyField('business_categories.BusinessCategory', related_name='business')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
