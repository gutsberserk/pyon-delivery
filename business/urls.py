from django import urls
from django.urls import path
from .views import AddAddressBusiness, CreateListBusinessView, ListAddCategoriesView, UpdateDeleteBusinessAdmin,ListBusinessBelongUser, UpdateDeleteBusinessFromUser

urlpatterns = [
    path("business/", CreateListBusinessView.as_view()),
    path("business/user/", ListBusinessBelongUser.as_view()),
    path("business/user/<str:business_id>/", UpdateDeleteBusinessFromUser.as_view()),
    path("business/admin/<str:business_id>/", UpdateDeleteBusinessAdmin.as_view()),
    path("business/address/<str:business_id>/", AddAddressBusiness.as_view()),
    path("business/categories/<str:business_id>/", ListAddCategoriesView.as_view()),
]