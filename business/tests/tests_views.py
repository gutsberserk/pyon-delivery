from uuid import uuid4
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from business.models import Business
from business_categories.models import BusinessCategory

from users.models import User
from utils.data.business_data import BusinessData
from utils.data.business_category_data import BusinessCategoryData
from utils.data.user_data import UserData


class TestBusinessRoutes(APITestCase):

    def test_manager_business_creation_returns_201(self):
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data

        response = self.client.post(
            '/api/business/', business_data
        )
        output = response.json()

        self.assertEqual(response.status_code, 201)
        self.assertEqual(output['name'], business_data['name'])
        self.assertIn('id', output)

    def test_manager_business_duplicate_creation_returns_400(self):
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data

        self.client.post('/api/business/', business_data)
        response = self.client.post(
            '/api/business/', business_data
        )

        self.assertEqual(response.status_code, 400)

    def test_manager_try_to_business_creation_with_invalid_data_returns_400(self):
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        del business_data['type']

        response = self.client.post(
            '/api/business/', business_data
        )

        self.assertEqual(response.status_code, 400)

    def test_any_user_try_to_business_creation_returns_403(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data

        response = self.client.post(
            '/api/business/', business_data
        )

        self.assertEqual(response.status_code, 403)

    def test_any_user_list_business_returns_200(self):
        manager_user = User.objects.create_user(**UserData.manager_user)
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        response = self.client.get('/api/business/', business_data)
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output[0]['name'], business.name)

    def test_manager_list_own_business_returns_200(self):
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        response = self.client.get('/api/business/user/')
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output[0]['name'], business.name)

    def test_admin_update_any_business_returns_200(self):
        manager_user = User.objects.create_user(**UserData.manager_user)
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        update_data = {
            'name': 'updated name'
        }

        response = self.client.patch(f'/api/business/admin/{business.id}/', update_data)
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output['name'], update_data['name'])

    def test_any_user_try_to_update_any_business_returns_403(self):
        manager_user = User.objects.create_user(**UserData.manager_user)
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        update_data = {
            'name': 'updated name'
        }

        response = self.client.patch(f'/api/business/admin/{business.id}/', update_data)

        self.assertEqual(response.status_code, 403)

    def test_admin_try_to_update_nonexistent_business_returns_404(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_id = '8888-8888-8888-8888'

        update_data = {
            'name': 'updated name'
        }

        response = self.client.patch(f'/api/business/admin/{business_id}/', update_data)

        self.assertEqual(response.status_code, 404)

    def test_manager_update_own_business_returns_200(self):
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        update_data = {
            'name': 'updated name'
        }

        response = self.client.patch(f'/api/business/user/{business.id}/', update_data)
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output['name'], update_data['name'])

    def test_manager_try_to_update_other_user_business_returns_403(self):
        manager_user = User.objects.create_user(**UserData.manager_user)
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=admin_user)

        update_data = {
            'name': 'updated name'
        }

        response = self.client.patch(f'/api/business/user/{business.id}/', update_data)

        self.assertEqual(response.status_code, 403)

    def test_manager_try_to_update_nonexistent_business_returns_404(self):
        manager_user = User.objects.create_user(**UserData.manager_user)
        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_id = '8888-8888-8888-8888'

        update_data = {
            'name': 'updated name'
        }

        response = self.client.patch(f'/api/business/user/{business_id}/', update_data)

        self.assertEqual(response.status_code, 404)

    def test_any_user_list_categories_of_any_business_returns_200(self):
        manager_user = User.objects.create_user(**UserData.manager_user)
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        category_data = BusinessCategoryData.restaurant_category_data
        category = BusinessCategory.objects.create(**category_data)

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)
        business.categories.add(category)

        response = self.client.get(f'/api/business/categories/{business.id}/')
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output['categories'][0]['name'], category.name)

    def test_any_user_try_to_list_categories_of_nonexistent_business_returns_404(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_id = uuid4()

        response = self.client.get(f'/api/business/categories/{business_id}/')

        self.assertEqual(response.status_code, 404)

    def test_manager_add_categories_to_an_owned_business_returns_200(self):
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        category_data = BusinessCategoryData.restaurant_category_data
        category = BusinessCategory.objects.create(**category_data)

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        put_data = {
           'categories': [category.name]
        }

        response = self.client.put(f'/api/business/categories/{business.id}/', put_data, format='json')
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output['categories'][0]['name'], category.name)

    def test_manager_try_to_add_categories_to_another_user_business_returns_403(self):
        manager_user = User.objects.create_user(**UserData.manager_user)
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        category_data = BusinessCategoryData.restaurant_category_data
        category = BusinessCategory.objects.create(**category_data)

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=admin_user)

        put_data = {
           'categories': [category.name]
        }

        response = self.client.put(f'/api/business/categories/{business.id}/', put_data, format='json')
        
        self.assertEqual(response.status_code, 403)

    def test_manager_try_to_add_categories_to_nonexistent_business_returns_404(self):
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        category_data = BusinessCategoryData.restaurant_category_data
        category = BusinessCategory.objects.create(**category_data)

        put_data = {
           'categories': [category.name]
        }

        business_id = uuid4()

        response = self.client.put(f'/api/business/categories/{business_id}/', put_data, format='json')
        
        self.assertEqual(response.status_code, 404)

    def test_any_user_try_to_add_categories_to_business_returns_403(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        category_data = BusinessCategoryData.restaurant_category_data
        category = BusinessCategory.objects.create(**category_data)

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=common_user)

        put_data = {
           'categories': [category.name]
        }

        response = self.client.put(f'/api/business/categories/{business.id}/', put_data, format='json')
        
        self.assertEqual(response.status_code, 403)

    def test_manager_try_to_add_categories_to_own_business_with_invalid_input_returns_400(self):
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        category_data = BusinessCategoryData.restaurant_category_data
        category = BusinessCategory.objects.create(**category_data)

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        put_data = {
           'categories': category.name
        }

        response = self.client.put(f'/api/business/categories/{business.id}/', put_data, format='json')
        
        self.assertEqual(response.status_code, 400)

        put_data = {
           'invalid': [category.name]
        }

        response = self.client.put(f'/api/business/categories/{business.id}/', put_data, format='json')
        
        self.assertEqual(response.status_code, 400)

    def test_manager_try_to_add_nonexistent_categories_to_own_business_returns_404(self):
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        put_data = {
           'categories': ['small market']
        }

        response = self.client.put(f'/api/business/categories/{business.id}/', put_data, format='json')
        
        self.assertEqual(response.status_code, 404)

    def test_manager_change_categories_to_an_owned_business_returns_200(self):
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=manager_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        category_data = BusinessCategoryData.restaurant_category_data
        category = BusinessCategory.objects.create(**category_data)

        new_category_data = BusinessCategoryData.market_category_data
        new_category = BusinessCategory.objects.create(**new_category_data)

        business_data = BusinessData.restaurant_data
        business = Business.objects.create(**business_data, user=manager_user)

        put_data = {
           'categories': [category.name]
        }

        put_new_data = {
           'categories': [new_category.name]
        }

        self.client.put(f'/api/business/categories/{business.id}/', put_data, format='json')
        response = self.client.put(f'/api/business/categories/{business.id}/', put_new_data, format='json')
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(output['categories']), 1)
        self.assertEqual(output['categories'][0]['name'], new_category.name)
