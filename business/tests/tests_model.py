from django.test import TestCase
from business.models import Business
from business_categories.models import BusinessCategory

from users.models import User
from utils.data.business_category_data import BusinessCategoryData 
from utils.data.user_data import UserData


class BusinessModelTest(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.user = User.objects.create_user(**UserData.manager_user)
        cls.business_category = BusinessCategory.objects.create(**BusinessCategoryData.restaurant_category_data)

        cls.name = 'example'
        cls.type = 'restaurant'
        cls.cnpj = '12345678000122'
        cls.corporate_email = 'example@mail.com'
        cls.website = 'example.com'
        cls.phone = '5511988880000'
        cls.active_employees = 12

        cls.business = Business.objects.create(
            name = cls.name,
            type = cls.type,
            cnpj = cls.cnpj,
            corporate_email = cls.corporate_email,
            website = cls.website,
            phone = cls.phone,
            active_employees = cls.active_employees,
            user = cls.user
        )

        cls.business.categories.add(cls.business_category)

    def test_business_fields(self):
        self.assertIsInstance(self.business.name, str)
        self.assertEqual(self.business.name, self.name)
        self.assertIsInstance(self.business.active_employees, int)
        self.assertEqual(self.business.active_employees, self.active_employees)
        self.assertEqual(self.business.categories.filter(name=self.business_category.name).first(), self.business_category)
        self.assertIsInstance(self.business.user, User)
        self.assertEqual(self.business.user.username, self.user.username)
