<div align="center">
    <h1>On Delivery system</h1>
    <h4>On Delivery system backend api using django framework<h4>
    <img src="https://arquivo.devmedia.com.br/cursos/imagem/curso_como-implementar-um-crud-em-django_38290.png" alt="Django image">
</div>

## Setup:

1. Clone the repository & `cd` to the root directory.

2. Install pre-commit and docker

- Pre Commit Hook (https://pre-commit.com/#install)
- Docker (https://docs.docker.com/engine/install/)

## Install pre-commit hooks

1. Just run the command bellow to install the hooks:

```
pre-commit install
```

## Running system in a container

1. Build the customized docker image:

```
docker build -t pyon-delivery .
```

2. Run the container:

```
docker run -it -p 8020:8020 pyon-delivery
```

The application will run in url `127.0.0.1:8020`

## Running system locally

1. Create a virtual environment with the command:

```
python -m venv venv
```

2. Enter in the virtual environment with the command:

```
source venv/bin/activate
```

3. Execute the command below to install the dependencies of the project:

```
pip install -r requirements.txt
```

4. Run the migrations:

```
python manage.py migrate
```

5. Run the server:

```
python manage.py runserver
```

The application will run in url `127.0.0.1:8000`

## Remarks

This template is developed and tested on

- Python 3.9
- Ubuntu 20.04.3 LTS
