from django.urls import path
from .views import GetOneAndUpdateAndDeleteUserView, LoginView, UserView

urlpatterns = [
    path('users/', UserView.as_view()),
    path("users/<str:pk>/", GetOneAndUpdateAndDeleteUserView.as_view()),
    path('login/', LoginView.as_view()),
]