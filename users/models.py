from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import BaseUserManager
from django.db import models
from django.utils import timezone
import uuid

from carts.models import Cart

class CustomUserManager(BaseUserManager):
    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):

        now = timezone.now()
        
        if not email:
            raise ValueError('The given email must be set')
        
        email = self.normalize_email(email)
        
        user = self.model(email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)

        user.set_password(password)

        user.save(using=self._db)

        return user
        
    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                         **extra_fields)
        
    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                     **extra_fields)

class User(AbstractUser):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    username = models.CharField(max_length=255, null=False, unique=True)
    first_name = models.CharField(max_length=255, null=False)
    last_name = models.CharField(max_length=255, null=False)
    email = models.EmailField(max_length=255, null=False, unique=True)
    phone = models.CharField(max_length=255, null=False, unique=True)
    cpf = models.CharField(max_length=255, null=False, unique=True)
    image_url = models.TextField()
    birthday_date = models.DateField()
    is_admin = models.BooleanField()
    is_manager = models.BooleanField()
    is_active = models.BooleanField(default=True)

    cart = models.OneToOneField(Cart, on_delete=models.CASCADE, null=True)

    address = models.ForeignKey("address.Address", on_delete=models.PROTECT, related_name="users", null=True)
    
    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = ['is_admin', 'is_manager', 'password']

    objects = CustomUserManager()