from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from users.models import User

from utils.data.user_data import UserData


class UserViewTest(APITestCase):

    def test_create_new_user_success(self):
        user_data = UserData.common_user

        response = self.client.post('/api/users/', user_data)

        self.assertEqual(response.status_code, 201)

        self.assertNotIn('password', response.json())

    def test_return_400_when_try_to_create_an_user_with_existent_email(self):
        user_data = UserData.common_user

        self.client.post('/api/users/', user_data)
        response = self.client.post('/api/users/', user_data)
        data = response.json()

        self.assertEqual(response.status_code, 400)

        self.assertEqual(data['email'], ['user with this email already exists.'])

    def test_return_400_when_try_to_create_an_user_with_invalid_email(self):
        user_data = UserData.common_user

        response = self.client.post('/api/users/', { **user_data, "email": "invalid_email"})
        data = response.json()

        self.assertEqual(response.status_code, 400)

        self.assertEqual(data['email'], ['Enter a valid email address.'])

    def test_return_400_when_try_to_create_an_user_with_invalid_username(self):
        user_data = UserData.common_user

        response = self.client.post('/api/users/', { **user_data, "username": "invalid@username!"})
        data = response.json()

        self.assertEqual(response.status_code, 400)

        self.assertEqual(data['username'], ['Enter a valid username.'])

    def test_return_400_when_try_to_create_an_user_with_invalid_phone(self):
        user_data = UserData.common_user

        response = self.client.post('/api/users/', { **user_data, "phone": "abcd"})
        data = response.json()

        self.assertEqual(response.status_code, 400)

        self.assertEqual(data['phone'], ['Enter a valid phone number.'])

    def test_return_400_when_try_to_create_an_user_with_invalid_cpf(self):
        user_data = UserData.common_user

        response = self.client.post('/api/users/', { **user_data, "cpf": "123a"})
        data = response.json()

        self.assertEqual(response.status_code, 400)

        self.assertEqual(data['cpf'], ['Enter a valid cpf.'])

    def test_return_400_when_try_to_create_an_user_with_invalid_data(self):
        user_data: dict = UserData.common_user
        del user_data['password']

        response = self.client.post('/api/users/', user_data)

        self.assertEqual(response.status_code, 400)

    def test_return_200_when_admin_list_all_users(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        response = self.client.get('/api/users/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()[0]['username'], 'admin')

    def test_return_403_when_common_user_try_to_list_all_users(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        response = self.client.get('/api/users/')

        self.assertEqual(response.status_code, 403)

    def test_return_200_when_any_user_get_own_data(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        response = self.client.get(f'/api/users/{common_user.id}/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['username'], 'user')

    def test_return_403_when_any_user_try_to_get_other_user_data(self):
        common_user = User.objects.create_user(**UserData.common_user)
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        response = self.client.get(f'/api/users/{manager_user.id}/')

        self.assertEqual(response.status_code, 403)

    def test_return_404_when_any_user_try_to_get_nonexistent_user_data(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        invalid_id = '8888-8888-8888-8888'

        response = self.client.get(f'/api/users/{invalid_id}/')

        self.assertEqual(response.status_code, 404)

    def test_return_200_when_any_user_update_own_data(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        update_data = {
            'username': 'username'
        }

        response = self.client.patch(f'/api/users/{common_user.id}/', update_data)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['username'], 'username')

    def test_return_403_when_any_user_try_to_update_other_user_data(self):
        common_user = User.objects.create_user(**UserData.common_user)
        manager_user = User.objects.create_user(**UserData.manager_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        update_data = {
            'username': 'username'
        }

        response = self.client.patch(f'/api/users/{manager_user.id}/', update_data)

        self.assertEqual(response.status_code, 403)

    def test_return_404_when_any_user_try_to_update_nonexistent_user_data(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        update_data = {
            'username': 'username'
        }

        invalid_id = '8888-8888-8888-8888'

        response = self.client.patch(f'/api/users/{invalid_id}/', update_data)

        self.assertEqual(response.status_code, 404)
        

class LoginViewTest(APITestCase):
    def setUp(self) -> None:
        User.objects.create_user(**UserData.common_user)

    def test_login_success(self):
        login_data = {
            'email': 'example@mail.com',
            'password': '1234'
        }

        response = self.client.post('/api/login/', login_data)

        self.assertEqual(response.status_code, 200)
        self.assertIn('token', response.json())

    def test_return_400_when_try_to_login_with_invalid_data(self):
        login_data = {
            'email': 'example@mail.com'
        }

        response = self.client.post('/api/login/', login_data)

        self.assertEqual(response.status_code, 400)

    def test_return_401_when_try_to_login_with_invalid_credentials(self):
        login_data = {
            'email': 'example@mail.com',
            'password': 'abcd'
        }

        response = self.client.post('/api/login/', login_data)

        self.assertEqual(response.status_code, 401)
