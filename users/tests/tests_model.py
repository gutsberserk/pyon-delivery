from datetime import date
from django.test import TestCase

from users.models import User


class UserModelTest(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.username = 'user'
        cls.first_name = 'test'
        cls.last_name = 'example'
        cls.email = 'example@mail.com'
        cls.phone = '5511933334444'
        cls.cpf = '44455566611'
        cls.password = '1234'
        cls.image_url = 'static/user.png'
        cls.birthday_date = date.today()
        cls.is_admin = True
        cls.is_manager = False

        cls.user = User.objects.create_user(
            username = cls.username,
            first_name = cls.first_name,
            last_name = cls.last_name,
            email = cls.email,
            phone = cls.phone,
            cpf = cls.cpf,
            password = cls.password,
            image_url = cls.image_url,
            birthday_date = cls.birthday_date,
            is_admin = cls.is_admin,
            is_manager = cls.is_manager
        )

    def test_user_fields(self):
        self.assertIsInstance(self.user.username, str)
        self.assertEqual(self.user.username, self.username)
        self.assertIsInstance(self.user.birthday_date, date)
        self.assertEqual(self.user.birthday_date, self.birthday_date)
        self.assertIsInstance(self.user.is_admin, bool)
        self.assertEqual(self.user.is_admin, self.is_admin)
