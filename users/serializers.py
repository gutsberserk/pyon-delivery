import re
from rest_framework import serializers
from .models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'password', 'first_name', 'last_name', 'phone', 'cpf', 'image_url', 'birthday_date', 'is_admin', 'is_manager', 'is_active']
        extra_kwargs = {
            'id': {'read_only': True, 'format': 'hex_verbose'},
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        user = User(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            phone=validated_data['phone'],
            cpf=validated_data['cpf'],
            image_url=validated_data['image_url'],
            birthday_date=validated_data['birthday_date'],
            is_admin=validated_data['is_admin'],
            is_manager=validated_data['is_manager']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

    def validate_username(self, username):
        if not re.match("^[\da-z]+$", username):
            raise serializers.ValidationError('Enter a valid username.')
        return username 

    def validate_cpf(self, cpf):
        if len(cpf) != 11:
            raise serializers.ValidationError('Enter a valid cpf.')
        return cpf

    def validate_phone(self, phone):
        if not re.match(r'^([\s\d]+)$', phone):
            raise serializers.ValidationError('Enter a valid phone number.')
        return phone 

class UserPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'image_url']
        extra_kwargs = {
            'id': {'read_only': True, 'format': 'hex_verbose'},
            'password': {'write_only': True}
        }
    

class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(write_only=True)
