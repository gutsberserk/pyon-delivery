from rest_framework import serializers
from orders.models import Order, OrderProduct
from products.serializers import ProductSerializer

class OrderSerializer(serializers.Serializer):
    id = serializers.UUIDField(read_only=True)
    anotation  = serializers.CharField(read_only=True)
    delivery_fee = serializers.FloatField(read_only=True)
    status = serializers.CharField(read_only=True)
    check_in = serializers.DateTimeField(read_only=True)
    check_out = serializers.DateTimeField(read_only=True)
    order_send = serializers.BooleanField(read_only=True)
    products = ProductSerializer(read_only=True, many=True)
    user_id = serializers.UUIDField(read_only=True)
    business_id = serializers.CharField(read_only=True)

    
    
    def create(self, validated_data):
        return Order.objects.create(**validated_data)
   
class OrderProductSerializerCreate(serializers.Serializer):
    id = serializers.UUIDField(read_only=True)
    orders_id = serializers.UUIDField()
    products_id = serializers.UUIDField()

    def create(self, validated_data):
        return OrderProduct.objects.create(**validated_data)

class OrderSendBusiness(serializers.Serializer):
    anotation  = serializers.CharField(required=False)

class OrderUpdateSerializer(serializers.Serializer):
    id = serializers.UUIDField(read_only=True)
    anotation  = serializers.CharField(required=False)
    delivery_fee = serializers.FloatField(required=False)
    status = serializers.CharField(required=False)
    check_in = serializers.DateTimeField(read_only=True)
    check_out = serializers.DateTimeField(required=False)
    order_send = serializers.BooleanField(read_only=True)
    user_id = serializers.UUIDField(read_only=True)