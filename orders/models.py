from typing import Tuple
from django.db import models
from django.utils import timezone
import uuid
from products.models import Product

class Order(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    annotation  = models.TextField(null=True)
    delivery_fee = models.FloatField(null=True)
    status = models.CharField(max_length=255, default="opening")
    check_in = models.DateTimeField(default=timezone.now)
    check_out = models.DateTimeField(null=True)
    products = models.ManyToManyField(Product, through='orders.OrderProduct')
    order_send = models.BooleanField(default=False, null=True)
    user = models.ForeignKey("users.User", null=True ,on_delete=models.CASCADE)
    business_id = models.CharField(max_length=255)

class OrderProduct(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    products = models.ForeignKey(Product, on_delete=models.CASCADE)
    orders = models.ForeignKey(Order, on_delete=models.CASCADE)