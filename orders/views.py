from django.http import Http404

from products.serializers import ProductSerializer
from .models import Order, OrderProduct
from .serializers import OrderSendBusiness, OrderSerializer, OrderUpdateSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import OrderSerializer, OrderProductSerializerCreate
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from carts.models import Cart, CartProduct
from business.models import Business
from products.models import Product
from carts.serializers import CartProductSerializer, CartSerializer

class CreateListOrdersView(APIView):

    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]

    def post(self, request):
        cart_id = request.user.cart_id
        
        cart_product = CartProduct.objects.filter(cart_id=cart_id)
        cart_product_serializer = CartProductSerializer(cart_product, many=True)
        
        if len(cart_product_serializer.data) == 0:
            return Response("O carrinho esta vazio")

        
        products = []
        for x in cart_product_serializer.data:
            product = Product.objects.get(id=x["product_id"])
            product_serializer = ProductSerializer(product)
            products.append(product_serializer.data)

        
        if len(products) == 0:
            return Response("O carrinho esta vazio")

        business_ids = set([product["business_id"] for product in products])
        
        for id in business_ids:
            order_serializer = OrderSerializer(data=request.data)
            if order_serializer.is_valid():
                order_serializer.save()

                order = Order.objects.get(id=order_serializer.data["id"])
                order.user = request.user
                order.save()
                
                product_filter = [product for product in products if product["business_id"] == id]
                for product in product_filter:

                    order_product_serializer = OrderProductSerializerCreate(data={"orders_id": order_serializer.data["id"], "products_id": product["id"]})
                    if order_product_serializer.is_valid():
                        order_product_serializer = OrderProduct.objects.create(**order_product_serializer.validated_data)
                        order_product_serializer = OrderProductSerializerCreate(order_product_serializer)

        return Response(order_serializer.data)

    def get(self, request):
        orders = Order.objects.all()
        orders_serializer = OrderSerializer(orders, many=True)

        return Response(orders_serializer.data)
            
class ListOrderBelongsUser(APIView):

    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    
    def get(self, request):

        user_id = request.user.id
        orders = Order.objects.filter(user_id=user_id)
        orders_serializer = OrderSerializer(orders, many=True)
    
        return Response(orders_serializer.data)

class SendOrderForBusinessView(APIView):
    
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]

    def post(self, request, order_id=''):

        request.data["order_send"] = True
        order_update = OrderSendBusiness(data=request.data)
        Order.objects.filter(id=order_id).update(**request.data)

        return Response({"message": "pedido enviado ao estabelecimento"})

class UpdateGetOneDeleteOrders(APIView):
    def patch(self, request, order_id=''):
        order_serializer_update = OrderUpdateSerializer(data=request.data)
        if order_serializer_update.is_valid():
            Order.objects.filter(id=order_id).update(**order_serializer_update.validated_data)
            
        order = Order.objects.get(id=order_id)
        order_serializer = OrderSerializer(order)

        return Response(order_serializer.data)

        
    def delete(self, request, order_id=''):
        order = Order.objects.get(id=order_id)
        order.delete()

        return Response(status=204)
    
    def get(self, request, order_id=''):

        order = Order.objects.get(id=order_id)
        order_serializer = OrderSerializer(order)

        return Response(order_serializer.data)


class BusinessListOrders(APIView):

    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]

    def get(self, request, business_id=''):

        try:
            business = Business.objects.filter(id=business_id).first()
            if not business:
                raise Http404
        except:
            raise Http404

        if request.user.id != business.user.id:
            return Response({'message': "You don't have permissions to do that."}, status=403)

        try:
            business_orders = Order.objects.filter(business_id=business_id)
            if not business_orders:
                raise Http404
        except:
            raise Http404

        serialized = OrderSerializer(business_orders, many=True)

        return Response(serialized.data)

