from django import urls
from django.urls import path
from .views import BusinessListOrders, CreateListOrdersView, ListOrderBelongsUser, UpdateGetOneDeleteOrders, SendOrderForBusinessView

urlpatterns = [
    path("orders/", CreateListOrdersView.as_view()),
    path("orders/user/", ListOrderBelongsUser.as_view()),
    path("orders/edit/<str:order_id>/", UpdateGetOneDeleteOrders.as_view()),
    path("orders/business/list/<business_id>/", BusinessListOrders.as_view()),
    path("orders/business/send/<order_id>/", SendOrderForBusinessView.as_view()),
]