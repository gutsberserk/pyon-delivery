from django.db import models
import uuid
from products.models import Product

class Cart(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    products = models.ManyToManyField(Product, related_name="carts", through='carts.CartProduct')
    
class CartProduct(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
 