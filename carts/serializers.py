from rest_framework import serializers

from products.serializers import ProductSerializer
from .models import CartProduct

class CartSerializer(serializers.Serializer):
    id = serializers.UUIDField()
    products = ProductSerializer(many=True, read_only=True)

class CartProductSerializer(serializers.Serializer):
    id = serializers.UUIDField(read_only=True)
    product_id = serializers.UUIDField()
    cart_id = serializers.UUIDField()

    def create(self, validate_data):
        return CartProduct.objects.create(**validate_data)
