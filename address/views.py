from address.permissions import IsAdmin
from .models import Address
from rest_framework import generics
from .serializers import AddressSerializer
from rest_framework.authentication import TokenAuthentication
class AddressCreateAndListView(generics.ListCreateAPIView):

    queryset = Address.objects.all()
    serializer_class = AddressSerializer

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdmin]

class AddressUpdateAndDelete(generics.RetrieveUpdateDestroyAPIView):
    
    queryset = Address.objects.all()
    serializer_class = AddressSerializer
    
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdmin]
    
    lookup_url_kwarg = "address_id"