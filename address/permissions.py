from rest_framework.permissions import BasePermission


class IsAdmin(BasePermission):

    def has_permission(self, request, view):
        if request.method == 'POST':
            return bool(request.user.is_authenticated == True)
        if request.method == 'DELETE' or request.method == 'PATCH' or request.method == 'GET': 
            return bool(request.user.is_authenticated and 
                request.user.is_admin == True)

        return True