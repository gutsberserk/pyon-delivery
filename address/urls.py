from django.urls import path
from .views import AddressCreateAndListView,AddressUpdateAndDelete

urlpatterns = [
    path('address/', AddressCreateAndListView.as_view()),
    path('address/<address_id>/', AddressUpdateAndDelete.as_view()),
]