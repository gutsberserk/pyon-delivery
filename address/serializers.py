from rest_framework import serializers
from .models import Address
from pyonDelivery.exceptions import AddressAlreadyExistsError,ZipCodeInvalid
import requests

class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields = '__all__'

    def validate(self, attrs):

        zip_code = attrs['zip_code']
        house_number = attrs['house_number']

        addr = Address.objects.filter(zip_code = zip_code, house_number=house_number).first()
        if addr:
            raise AddressAlreadyExistsError()
        try:
            req = requests.get(f"https://viacep.com.br/ws/{zip_code}/json/").json()
        except:
            raise ZipCodeInvalid()

        return super().validate(attrs)

    def create(self, validated_data):
        address = Address.objects.create(**validated_data)
        return address
