from rest_framework.serializers import ModelSerializer

from pyonDelivery.exceptions import CategoryAlreadyExistsError
from .models import BusinessCategory


class BusinessCategorySerializer(ModelSerializer):
    class Meta:
        model = BusinessCategory
        fields = '__all__'

        extra_kwargs = {'name': {'validators': []}}

    def validate(self, attrs):

        attrs['name'] = attrs['name'].lower()

        name = attrs['name']

        category = BusinessCategory.objects.filter(name=name).first()

        if category:
            raise CategoryAlreadyExistsError()

        return super().validate(attrs)

    def create(self, validated_data):
        return BusinessCategory.objects.create(**validated_data)
