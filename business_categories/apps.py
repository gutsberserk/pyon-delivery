from django.apps import AppConfig


class BusinessCategoriesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'business_categories'
