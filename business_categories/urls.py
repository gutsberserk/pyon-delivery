from django.urls import path
from .views import BusinessCategoryCreateAndListView, BusinessCategoryEditView, BusinessCategoryFilterByNameView

urlpatterns = [
    path('business-categories/', BusinessCategoryCreateAndListView.as_view()),
    path(
        'business-categories/filter/id/<business_category_id>/',
        BusinessCategoryEditView.as_view(),
    ),
    path(
        'business-categories/filter/name/<business_category_name>/',
        BusinessCategoryFilterByNameView.as_view(),
    ),
]
