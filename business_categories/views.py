from django.http import Http404
from pyonDelivery.permissions import OnlyAdmin, OnlyAdminCreateAndManagerList, OnlyAdminOrManager
from .models import BusinessCategory
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.views import APIView
from .serializers import BusinessCategorySerializer


class BusinessCategoryCreateAndListView(ListCreateAPIView):

    queryset = BusinessCategory.objects.all()
    serializer_class = BusinessCategorySerializer

    permission_classes = [OnlyAdminCreateAndManagerList]
    authentication_classes = [TokenAuthentication]


class BusinessCategoryEditView(RetrieveUpdateDestroyAPIView):

    queryset = BusinessCategory.objects.all()
    serializer_class = BusinessCategorySerializer
    lookup_url_kwarg = 'business_category_id'

    permission_classes = [OnlyAdmin]
    authentication_classes = [TokenAuthentication]

class BusinessCategoryFilterByNameView(APIView):

    permission_classes = [OnlyAdminOrManager]
    authentication_classes = [TokenAuthentication]

    def get(self, request, *args, **kwargs):

        business_category_name = kwargs['business_category_name']

        
        business_category = BusinessCategory.objects.filter(name=business_category_name).first()
        if not business_category:
            raise Http404

        business_category_serializer = BusinessCategorySerializer(business_category)

        return Response(business_category_serializer.data)
