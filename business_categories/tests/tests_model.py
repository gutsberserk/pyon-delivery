from django.test import TestCase

from business_categories.models import BusinessCategory


class BusinessCategoryModelTest(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.name = 'italian restaurant'
        cls.description = 'A small italian restaurant'

        cls.business_category = BusinessCategory.objects.create(
            name = cls.name,
            description = cls.description
        )

    def test_business_category_fields(self):
        self.assertIsInstance(self.business_category.name, str)
        self.assertEqual(self.business_category.name, self.name)
