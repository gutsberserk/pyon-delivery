from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from business_categories.models import BusinessCategory

from users.models import User
from utils.data.business_category_data import BusinessCategoryData
from utils.data.user_data import UserData


class TestBusinessCategoriesRoutes(APITestCase):

    def test_admin_business_category_creation_returns_201(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_category_data = BusinessCategoryData.restaurant_category_data
        # Creating a business category
        response = self.client.post(
            '/api/business-categories/', business_category_data
        )
        output = response.json()

        self.assertEqual(response.status_code, 201)
        self.assertEqual(output['name'], business_category_data['name'])
        self.assertEqual(
            output['description'], business_category_data['description']
        )
        self.assertIn('id', output)

    def test_any_user_try_to_business_category_creation_returns_403(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_category_data = BusinessCategoryData.restaurant_category_data
        # Creating a business category
        response = self.client.post(
            '/api/business-categories/', business_category_data
        )

        self.assertEqual(response.status_code, 403)

    def test_duplicate_category_name_returns_422(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_category_data = BusinessCategoryData.restaurant_category_data
        # Trying to create two categories with the same name
        self.client.post(
            '/api/business-categories/', business_category_data
        )
        response = self.client.post(
            '/api/business-categories/', business_category_data
        )
        output = response.json()

        self.assertEqual(response.status_code, 422)
        self.assertEqual(output, {'message': 'Category already exists'})

    def test_convert_business_category_name_to_lower_case(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_category_data = {
            'name': 'EXAmple',
            'description': 'An simple case convertion test'
        }
        # Creating a business category
        response = self.client.post(
            '/api/business-categories/', business_category_data
        )
        output = response.json()

        self.assertEqual(response.status_code, 201)
        self.assertEqual(output['name'], business_category_data['name'].lower())

    def test_admin_list_all_business_categories_returns_200(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_category = BusinessCategory.objects.create(**BusinessCategoryData.restaurant_category_data)

        response = self.client.get('/api/business-categories/')
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output[0]['name'], business_category.name)

    def test_any_user_try_to_list_all_business_categories_returns_403(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        BusinessCategory.objects.create(**BusinessCategoryData.restaurant_category_data)

        response = self.client.get('/api/business-categories/')

        self.assertEqual(response.status_code, 403)

    def test_admin_update_a_business_category_returns_200(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_category = BusinessCategory.objects.create(**BusinessCategoryData.restaurant_category_data)
        update_data = {
            'name': 'french restaurant'
        }

        response = self.client.patch(f'/api/business-categories/filter/id/{business_category.id}/', update_data)
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output['name'], update_data['name'])

    def test_update_a_business_category_with_existent_name_returns_422(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        BusinessCategory.objects.create(**BusinessCategoryData.market_category_data)
        business_category = BusinessCategory.objects.create(**BusinessCategoryData.restaurant_category_data)
        update_data = {
            'name': 'hypermarket'
        }

        response = self.client.patch(f'/api/business-categories/filter/id/{business_category.id}/', update_data)

        self.assertEqual(response.status_code, 422)

    def test_any_user_try_to_update_a_business_category_returns_403(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_category = BusinessCategory.objects.create(**BusinessCategoryData.restaurant_category_data)
        update_data = {
            'name': 'french restaurant'
        }

        response = self.client.patch(f'/api/business-categories/filter/id/{business_category.id}/', update_data)

        self.assertEqual(response.status_code, 403)

    def test_admin_filter_a_business_category_by_id_returns_200(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_category = BusinessCategory.objects.create(**BusinessCategoryData.restaurant_category_data)

        response = self.client.get(f'/api/business-categories/filter/id/{business_category.id}/')
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output['name'], business_category.name)

    def test_try_to_filter_a_business_category_with_invalid_id_returns_404(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_category_id = '8888-8888-8888-8888'

        response = self.client.get(f'/api/business-categories/filter/id/{business_category_id}/')
        self.assertEqual(response.status_code, 404)

    def test_admin_filter_a_business_category_by_name_returns_200(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_category = BusinessCategory.objects.create(**BusinessCategoryData.restaurant_category_data)

        response = self.client.get(f'/api/business-categories/filter/name/{business_category.name}/')
        output = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(output['description'], business_category.description)

    def test_admin_filter_a_business_category_with_invalid_name_returns_404(self):
        admin_user = User.objects.create_user(**UserData.admin_user)

        token = Token.objects.create(user=admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_category_name = 'franchise'

        response = self.client.get(f'/api/business-categories/filter/name/{business_category_name}/')

        self.assertEqual(response.status_code, 404)

    def test_any_user_try_to_filter_a_business_category_by_name_returns_403(self):
        common_user = User.objects.create_user(**UserData.common_user)

        token = Token.objects.create(user=common_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        business_category = BusinessCategory.objects.create(**BusinessCategoryData.restaurant_category_data)

        response = self.client.get(f'/api/business-categories/filter/name/{business_category.name}/')

        self.assertEqual(response.status_code, 403)
