from dataclasses import fields
from pyexpat import model
from rest_framework import serializers

from users.serializers import UserPostSerializer
from .models import Post

class PostSerializer(serializers.ModelSerializer):

    user = UserPostSerializer(read_only=True)
    
    class Meta():
        model = Post
        fields = '__all__'

        extra_kwargs = {
            'id': {'read_only': True, 'format': 'hex_verbose'},
            'user': {'required': False}
        }