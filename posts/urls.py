from django.urls import path
from .views import GetOneAndUpdateAndDeletePostView, CreateAndListPostView

urlpatterns = [
    path("posts/", CreateAndListPostView.as_view()),
    path("posts/<str:pk>/", GetOneAndUpdateAndDeletePostView.as_view())
]