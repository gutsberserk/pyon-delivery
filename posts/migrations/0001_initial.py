# Generated by Django 4.0.3 on 2022-04-01 01:23

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('comments', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('text', models.TextField()),
                ('image_url', models.CharField(max_length=255)),
                ('likes', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('visibility', models.CharField(max_length=255)),
                ('comments', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='post', to='comments.comment')),
            ],
        ),
    ]
