from django.db import models
import uuid

class Post(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    text = models.TextField(null=False)
    image_url = models.CharField(max_length=255)
    likes = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    visibility = models.CharField(max_length=255, null=False)

    user = models.ForeignKey("users.User", on_delete=models.PROTECT, related_name="posts")

    comments = models.ForeignKey("comments.Comment", on_delete=models.PROTECT, related_name="post", null=True)
