from django.db import models
import uuid
from django.utils import timezone

class Comment(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    body = models.TextField()
    likes = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now)
