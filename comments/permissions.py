from rest_framework.permissions import BasePermission

class IsAdmin(BasePermission):

    def has_permission(self, request, view):
        if request.method == 'POST':
            return bool(request.user.is_authenticated == True)
        if request.method == 'DELETE' or request.method == 'PATCH': 
            if bool(request.user.is_authenticated == True):
                return True
        if request.method == 'GET':
            return bool(request.user.is_authenticated and request.user.is_admin == True)

        return False