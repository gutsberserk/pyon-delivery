from django.http import HttpResponseRedirect

from users.models import User
from .models import Comment
from rest_framework import generics
from .serializers import CommentSerializer
from .permissions import IsAdmin
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from rest_framework import status

class CommentCreateAndListView(generics.ListCreateAPIView):

    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdmin]

class CommentDetailView(generics.RetrieveUpdateDestroyAPIView): 
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    lookup_url_kwarg = "comment_id"