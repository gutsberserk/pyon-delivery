from django.urls import path
from .views import CommentCreateAndListView,CommentDetailView

urlpatterns = [
    path('comment/', CommentCreateAndListView.as_view()),
    path('comment/<comment_id>/', CommentDetailView.as_view()),
]