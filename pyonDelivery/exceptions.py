from rest_framework.exceptions import APIException


class AddressAlreadyExistsError(APIException):
    status_code = 409
    default_detail = {'error': ['This address is already registered.']}


class ZipCodeInvalid(APIException):
    status_code = 400
    default_detail = {'error': ['Invalid zip_code.']}


class CategoryAlreadyExistsError(APIException):
    status_code = 422
    default_detail = {'message': 'Category already exists'}


class OrderStatusIsConcluded(APIException):
    status_code = 400
    default_detail = {'error': ['The order cannot be deleted because the order status is concluded.']}


class OrderNotFound(APIException):
    status_code = 404
    default_detail = {'error': ['Order not found.']}


class InvalidRatingRangeException(APIException):
    status_code = 400
    default_detail = {'error': ['Rating needs to be between 1 and 5.']}
