from django.http import Http404
from rest_framework.permissions import BasePermission
from business.models import Business
from payment_methods.models import PaymentMethod
from reviews.models import Review

from users.models import User


class HandleOnlyOwnData(BasePermission):
    
    def has_permission(self, request, view):

        kwargs = view.__dict__['kwargs']

        user_id = kwargs['pk']

        try:
            user = User.objects.filter(id=user_id).first() 
            if not user:
                raise Http404 
        except: 
            raise Http404

        return request.user and request.user.is_authenticated and request.user.id == user.id


class OnlyAdmin(BasePermission):
    
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.is_admin == True


class OnlyAdminCreateAndManagerList(BasePermission):
    
    def has_permission(self, request, view):
        if request.method == 'GET':
            return request.user and request.user.is_authenticated and (request.user.is_admin == True or request.user.is_manager == True)
        return request.user and request.user.is_authenticated and request.user.is_admin == True
        

class OnlyAdminOrManager(BasePermission):
    
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and (request.user.is_admin == True or request.user.is_manager == True) 


class OnlyManagerCreate(BasePermission):
    
    def has_permission(self, request, view):
        if request.method == 'POST':
            return request.user and request.user.is_authenticated and request.user.is_manager == True

        return request.user and request.user.is_authenticated


class OnlyOwner(BasePermission):
    
    def has_permission(self, request, view):

        kwargs = view.__dict__['kwargs']

        business_id = kwargs['business_id']

        try:
            business = Business.objects.filter(id=business_id).first() 
            if not business:
                raise Http404 
        except: 
            raise Http404 

        return request.user and request.user.is_authenticated and request.user.id == business.user.id

class OnlyManagerOwnerAddCategory(BasePermission):
    
    def has_permission(self, request, view):
        
        if request.method == 'PUT':
            kwargs = view.__dict__['kwargs']

            business_id = kwargs['business_id']

            try:
                business = Business.objects.filter(id=business_id).first() 
                if not business:
                    raise Http404 
            except: 
                raise Http404 

            return request.user and request.user.is_authenticated and request.user.is_manager == True and request.user.id == business.user.id

        return True


class OnlyCommonUserCreateEditReview(BasePermission):
    
    def has_permission(self, request, view):
        if request.method == 'GET':
            return True
        
        return request.user and request.user.is_authenticated and request.user.is_admin == False and request.user.is_manager == False


class OnlyReviewOwner(BasePermission):
    
    def has_permission(self, request, view):

        kwargs = view.__dict__['kwargs']

        review_id = kwargs['review_id']

        try:
            review = Review.objects.filter(id=review_id).first() 
            if not review:
                raise Http404 
        except: 
            raise Http404 

        return request.user and request.user.is_authenticated and request.user.id == review.user.id


class OnlyReviewBusinessOwner(BasePermission):
    
    def has_permission(self, request, view):

        kwargs = view.__dict__['kwargs']

        review_id = kwargs['review_id']

        try:
            review = Review.objects.filter(id=review_id).first() 
            if not review:
                raise Http404 
        except: 
            raise Http404 

        return request.user and request.user.is_authenticated and request.user.id == review.business.user.id


class OnlyCommonUserCreateEditPaymentMethods(BasePermission):
    
    def has_permission(self, request, view):
        if request.method == 'GET':
            return True
        
        return request.user and request.user.is_authenticated and request.user.is_admin == False and request.user.is_manager == False


class OnlyPaymentMethodOwner(BasePermission):
    
    def has_permission(self, request, view):

        kwargs = view.__dict__['kwargs']

        payment_method_id = kwargs['payment_method_id']

        try:
            payment_method = PaymentMethod.objects.filter(id=payment_method_id).first() 
            if not payment_method:
                raise Http404 
        except: 
            raise Http404 

        return request.user and request.user.is_authenticated and request.user.id == payment_method.user.id
