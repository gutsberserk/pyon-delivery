from django.urls import path
from .views import ProductCategorieCreateAndListView,ProductCategorieDetailView

urlpatterns = [
    path('product_categorie/', ProductCategorieCreateAndListView.as_view()),
    path('product_categorie/<product_categorie_id>/', ProductCategorieDetailView.as_view()),
]