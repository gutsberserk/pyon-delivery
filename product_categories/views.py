from .models import ProductCategorie
from rest_framework import generics
from .serializers import ProductCategorieSerializer
from .permissions import IsAdmin
from rest_framework.authentication import TokenAuthentication

class ProductCategorieCreateAndListView(generics.ListCreateAPIView):

    queryset = ProductCategorie.objects.all()
    serializer_class = ProductCategorieSerializer

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdmin]

class ProductCategorieDetailView(generics.RetrieveUpdateDestroyAPIView): 
    queryset = ProductCategorie.objects.all()
    serializer_class = ProductCategorieSerializer
    lookup_url_kwarg = "product_categorie_id"

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdmin]