from rest_framework import serializers
from .models import ProductCategorie

class ProductCategorieSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductCategorie
        fields = '__all__'