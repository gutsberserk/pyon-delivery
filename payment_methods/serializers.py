from rest_framework.serializers import ModelSerializer

from payment_methods.models import PaymentMethod
from users.serializers import UserSerializer


class PaymentMethodSerializer(ModelSerializer):

    user = UserSerializer(read_only=True)

    class Meta:
        model = PaymentMethod
        fields = '__all__'

    def validate(self, attrs):

        attrs['user'] = self.context['request'].user

        return super().validate(attrs)

    def create(self, validated_data):
        return PaymentMethod.objects.create(**validated_data)
