from django.db import models
import uuid


class PaymentMethod(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField(max_length=255)
    card_number = models.CharField(max_length=255)
    expire_date = models.DateField()
    card_owner = models.CharField(max_length=255)
    security_code = models.CharField(max_length=255)
    user = models.ForeignKey("users.User", on_delete=models.CASCADE)
    