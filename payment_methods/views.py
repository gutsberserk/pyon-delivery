from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.views import APIView
from rest_framework.response import Response

from payment_methods.models import PaymentMethod
from payment_methods.serializers import PaymentMethodSerializer
from pyonDelivery.permissions import OnlyCommonUserCreateEditPaymentMethods, OnlyPaymentMethodOwner


class PaymentMethodCreateAndListView(ListCreateAPIView):

    queryset = PaymentMethod.objects.all()
    serializer_class = PaymentMethodSerializer

    permission_classes = [OnlyCommonUserCreateEditPaymentMethods]
    authentication_classes = [TokenAuthentication]


class PaymentMethodEditView(RetrieveUpdateDestroyAPIView):

    queryset = PaymentMethod.objects.all()
    serializer_class = PaymentMethodSerializer
    lookup_url_kwarg = 'payment_method_id'

    permission_classes = [OnlyPaymentMethodOwner]
    authentication_classes = [TokenAuthentication]

class ListAllUserPaymentMethodsView(APIView):

    authentication_classes = [TokenAuthentication]

    def get(self, request):

        payment_methods = PaymentMethod.objects.filter(user_id=request.user.id).all()

        serialized = PaymentMethodSerializer(payment_methods, many=True)

        return Response(serialized.data)
