from django.urls import path

from payment_methods.views import ListAllUserPaymentMethodsView, PaymentMethodCreateAndListView, PaymentMethodEditView

urlpatterns = [
    path('payment-methods/', PaymentMethodCreateAndListView.as_view()),
    path(
        'payment-methods/user/<payment_method_id>/',
        PaymentMethodEditView.as_view(),
    ),
    path('payment-methods/user/', ListAllUserPaymentMethodsView.as_view()),
]
